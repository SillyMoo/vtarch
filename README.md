# VTArch

This repository defines an architecture for the Virus Total website.

Architectural design can be found in [architecture.md](architecture.md)
