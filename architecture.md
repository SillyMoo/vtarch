# Architecture

## High level

```plantuml

!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Context.puml

LAYOUT_LEFT_RIGHT()
LAYOUT_WITH_LEGEND()

Person(User, "User", "Website user")
Person(ApiUser, "API User" )

System_Ext(ApiGateway, "Api Gateway", "Api Gateway")

System(VTotal, "Virus Total")

System_Ext(FileStore, "FileStore", "File Storage")
System_Ext(DataStore, "DataStore", "Key Value Storage")
System_Ext(AuthService, "AuthService", "Authentication Service")

Rel(User, VTotal, "Uses")
Rel(ApiUser, ApiGateway, "Uses")
Rel(ApiGateway, VTotal, "Uses")
Rel(ApiGateway, AuthService, "Api Authentication")

Rel(VTotal, FileStore, "Add/Read files")
Rel(VTotal, DataStore, "File info and Metadata")
Rel(VTotal, AuthService, "WebApp authentication")
```

A user can interact with the system via either the VT website, or the VT API. The VT API is managed by an API Gateway (such as Apigee or Amazon API Gateway). The API gateway and the website shall use an external authentication service (such as AWS cognito) to manage user authentication.

A verified user can upload a file for scanning to Virus Total, Virus Total shall store that file in a file store, detonate the sample in various Antivirus products, and use a Data Store to keep metadata associated with the file.

## Sub components

```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

LAYOUT_LEFT_RIGHT()
LAYOUT_WITH_LEGEND()

Person(User, "User", "Website user")
Person(ApiUser, "API User" )
System_Ext(ApiGateway, "Api Gateway", "Api Gateway")

System_Boundary(VTotal, "Virus Total") {
    Container(WebSite, "Website", "Go + HTML")

    Container(Rescan, "Rescanner", "Go")
    Container(Uploader, "Uploader", "Go")
    Container(Detonation, "Detonation", "Go, hypervisor, scripts/AV")
    Container(MetadataManager, "Metadata Manager", "Go")
}

System_Ext(FileStore, "FileStore", "File Storage")
System_Ext(DataStore, "DataStore", "Key Value Storage")
System_Ext(AuthService, "AuthService", "Authentication Service")

Rel(User, WebSite, "Uses", "Http")
Rel(WebSite, Uploader, "Request upload", "Http")
Rel(WebSite, MetadataManager, "Retrieve Metadata")
Rel_L(WebSite, AuthService, "Authentication")

Rel(ApiUser, ApiGateway, "Uses", "Http")
Rel(ApiGateway, Uploader, "Request upload", "Http")
Rel_R(ApiGateway, AuthService, "Authentication")
Rel(ApiGateway, MetadataManager, "Retrieve Metadata")

Rel(Uploader, FileStore, "Push File")
Rel(Uploader, MetadataManager, "Initial file metadata")
Rel(MetadataManager, DataStore, "Uses")

Rel(Detonation, FileStore, "Retrieve file")
Rel(Detonation, MetadataManager, "Update")

Rel_L(Uploader, Detonation, "Trigger", "MQ")
Rel_R(Rescan, Detonation, "Trigger", "MQ")
```

The VT [Website](website.md) allows a user to upload a file via the [uploader service](uploader.md), or browse metadata via the [Metadata Manager service](metadata_manager.md). The VT API allows the same functionality, but via a restful API.

The [uploader service](uploader.md) will push a new file to the data store, create some initial data for that file (via the [Metadata Manager](metadata_manager.md)) and then request [detonation](detonation.md). A [rescan service](rescanner.md) can request that files be rescanned when new scripts or AntiVirus vendors/versions are added to the system.

The [detonation service](detonation.md) will run a set of scripts or antivirus on a sample, it will do this within a virtual machine, to ensure the system is protected from executing potential malware. Whenever a detonation is requested the [detonation service](detonation.md) shall pull the requested file from the file store, detonate the file in one or more virtual machines, and store any resultant metadata in the [Metadata manager](metadata_manager.md). Since detonation is a potentially time consuming exercise access to the detonation manager is via a queue.

The [Metadata Manager](metadata_manager.md) is responsible for storing and retrieving metadata about a sample.

## Questions:
* Do we need a separate component(s) for static scanning?
