# Metadata manager

The metadata manager provides a consistent API to the backend datastore, allowing components to specify or retrieve the metadata for an individual file.

The initial creation of a files metadata must include properties such as:
* MD5
* SHA-1
* SHA-256
* File size
* File type
* List of Av-engines with a list of ID's per engine (+timestamp)

Associated with each file is a number of Detections (the AV engine that detected the file, how the AV reported the file), and a set of Behaviours. The Detections shall be versioned, to ensure that any history is retained (i.e when AV was updated and started to detect file).

# API
Still in progress, example endpoints:


POST /file <br/>
Takes initial metadata in body (application/json), returns newly created file metadata json (includes ID to help identify in later APIs)

GET /file/{ID} <br/>
Returns file json, includes links to Detections and Behaviours.

POST /file/{ID}/detections/{AVEngine} <br/>
Takes AV detection details in body (application/json), returns newly created detection metadata json.

GET /file/{ID}/detections/{AVEngine}/{ID} <br/>
GET /file/{ID}/detections/{AVEngine}/latest <br/>
Returns detection metadata json, note versioned hence support to retrieve by ID or latest

POST /file/{id}/behaviours/{Type} <br/>
Takes behaviours in body (application/json), returns newly created behaviour metadata.

GET /file/{ID}/behaviours/{Type}/{ID} <br/>
GET /file/{ID}/behaviours/{Type}/latest <br/>
Returns behaviour metadata json, note versioned hence support to retrieve by ID or latest


