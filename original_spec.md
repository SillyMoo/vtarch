The original task definition:

Virus total whiteboard project

The whiteboard project will be building a “VirusTotal” clone. This is a made up project that
represents scenarios that are realistic.<br/>
https://www.virustotal.com/en/about/

Your role will be to understand what VirusTotal does and propose an architecture that will
accomplish the following goals:

● Ability for users to upload files and be shown meta-data about that file. E.g. we run
scripts and virus scanners on the files to extract information,<br/>
○ How does that work?<br/>
○ What kind of workflow exists there?<br/>
○ How and where will we store the data?<br/>
○ How will we handle failure in the system?<br/>

● What Statistics can we track on uploads, internal metrics for understanding system<br/>
health, etc…
○ Where’s a good place to store those?<br/>
○ What kind of metrics do we want to track?<br/>

● We’d like a 3rd party API that users can build apps on.<br/>
○ What are some of our endpoints<br/>
○ What auth do we need?<br/>
○ Provide some sample api request/response example<br/>
○ What does the url look like, what does the response look like?<br/>

This mock project will have an expected scale of millions of users per day so it needs to be built
with scalability in mind. We’d like you to be as detailed as possible without needing to get into
any actual code. Talk about the services we need built, how we’ll process files efficiently, data
routing, sharding, partitioning of data and the metrics and operations info we’ll collect to ensure
the system is running properly.<br/>
What are we looking for?<br/>
Do you have sound ideas, can we work together on a whiteboard, can you translate what’s in
your head into pictures that communicate your intentions? Also, for your own decision making,
did you enjoy white boarding with us?<br/>
Requirements:<br/>
● we want to keep files around forever if possible so we need a highly available storage
system for the raw uploaded files<br/>
● assume we have meta data script writers churning out new scripts to extract meta data
from upload files, we want to be able to add new scripts on the fly vs having a big
deployment process<br/>
Constraints:<br/>
● files could range from 100K to 1GB in size that are uploaded<br/>
● system has to be as realtime as possible<br/>
● Metadata and scanning services could run on a mixture of linux or windows. (Not all
scanning services support the same OS.)<br/>
Here is an example of meta data collected from file uploads
https://www.virustotal.com/en/file/0bf01094f5c699046d8228a8f5d5754ea454e5e58b4e6151fef3
7f32c83f6497/analysis/<br/>
